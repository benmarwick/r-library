#!/usr/bin/env bash

REPOLIST=$(mktemp)
URLLIST=$(mktemp)
ROOTDIR=$(git rev-parse --show-toplevel)

echo "Generate remote files list"
curl -s "https://bitbucket.org/psylab/r-library/downloads" | awk -F '"' '/\.pdf|\.djvu|\.zip/ { gsub(/\/psylab/, "http://git.psylab.info"); print($6) }' | awk 'NF' | sort -u > ${REPOLIST}

echo "Generate loca URLs list"
cat "${ROOTDIR}"/bibs/*.bib | awk -F '[{}]' '/\.pdf|\.djvu|\.zip/ {gsub(/[,\ ]/, ""); print ($3"\n"$5)}' | awk 'NF' | sort -u > ${URLLIST}

sdiff -s ${REPOLIST} ${URLLIST}

rm ${REPOLIST} ${URLLIST}
